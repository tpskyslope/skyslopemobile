import {ActionTypes} from './actions';
import update from 'immutability-helper';

export interface DigiSignState {
  signers: any[];
  documents: any[];
  blocks: any[];
  currentFile?: any;
}

export const initialState: DigiSignState = {
  signers: [],
  documents: [],
  blocks: [],
  currentFile: undefined,
};

export default (state: DigiSignState = initialState, action: any) => {
  switch (action.type) {
    case ActionTypes.RESET_STATE: {
      return initialState;
    }
    case ActionTypes.ADD_DOCUMENT: {
      return update(state, {
        documents: {$set: [...state.documents, action.payload]},
      });
    }
    case ActionTypes.ADD_SIGNER: {
      return update(state, {
        signers: {$set: [...state.signers, action.payload]},
      });
    }
    case ActionTypes.ADD_BLOCK: {
      return update(state, {
        blocks: {$set: [...state.blocks, action.payload]},
      });
    }
    case ActionTypes.SET_FILE: {
      return update(state, {
        currentFile: {$set: action.payload},
      });
    }
    default: {
      return state;
    }
  }
};
