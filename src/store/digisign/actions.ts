import {action} from 'typesafe-actions';

export enum ActionTypes {
  ADD_DOCUMENT = 'DigiSignActions/ADD_DOCUMENT',
  ADD_SIGNER = 'DigiSignActions/ADD_SIGNER',
  ADD_BLOCK = 'DigiSignActions/ADD_BLOCK',
  SET_FILE = 'DigiSignActions/SET_FILE',
  RESET_STATE = 'DigiSignActions/RESET_STATE',
}

export const addDocument = (document: any) =>
  action(ActionTypes.ADD_DOCUMENT, document);
export const addSigner = (signer: any) =>
  action(ActionTypes.ADD_SIGNER, signer);
export const addBlock = (block: any) => action(ActionTypes.ADD_BLOCK, block);
export const setCurrentFile = (file: any) => action(ActionTypes.SET_FILE, file);
export const resetState = () => action(ActionTypes.RESET_STATE);
