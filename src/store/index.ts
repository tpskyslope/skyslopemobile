import {
  createStore,
  Store,
  Action,
  Middleware,
  applyMiddleware,
  combineReducers,
} from 'redux';
import {logger} from 'redux-logger';
import authReducer, {AuthState} from './auth';
import digisignReducers, {DigiSignState} from './digisign';

const appReducer = combineReducers({
  authState: authReducer,
  digiSignState: digisignReducers,
});

export interface RootState {
  authState: AuthState;
  digiSignState: DigiSignState;
}

const rootReducer = (state: any, action: any): RootState => {
  return appReducer(state, action);
};

const configureStore = (
  initialState = {},
): Store<RootState, Action<string>> => {
  const middleware: Middleware[] = [];
  middleware.push(logger);

  const enhancers = [applyMiddleware(...middleware)];

  // @ts-ignore
  const store = createStore(rootReducer, initialState, ...enhancers);
  return store;
};

export default configureStore;
