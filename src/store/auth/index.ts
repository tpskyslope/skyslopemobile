import update from 'immutability-helper';
import {action} from 'typesafe-actions';

export enum AuthAction {
  SET_AUTH_TOKEN = 'AuthAction/SET_AUTH_TOKEN',
}

export const setAuthToken = (
  token?: string,
): {type: AuthAction.SET_AUTH_TOKEN; payload?: string} =>
  action(AuthAction.SET_AUTH_TOKEN, token);

export interface AuthState {
  accessToken?: string;
}

const initialState: AuthState = {
  accessToken: undefined,
};

const reducer = (state: AuthState = initialState, action: any): AuthState => {
  switch (action.type) {
    case AuthAction.SET_AUTH_TOKEN: {
      return update(state, {
        accessToken: {$set: action.payload},
      });
    }
    default: {
      return state;
    }
  }
};

export default reducer;
