import wirefraudBase64 from './mock/wirefraudBase64';

const createEnvelopePayload = (
  address: string,
  signers: any[],
  blocks: any[],
) => {
  const payload = {
    subject: address,
    body: `Signature for ${address}`,
    isReadyForRecipients: true,
    expiration: '2020-12-20',
    config: {},
    signers: signers,
    documents: [
      {
        fileName: 'WireSafe-Fraud.pdf',
        base64Data: wirefraudBase64,
        blocks: [
          ...blocks,
          {
            pageNumber: 0,
            blockType: 'Text',
            x: 37,
            y: 652,
            width: 255,
            height: 17,
            fontSize: 14,
            defaultValue: address,
            value: address,
            readOnly: true,
            isEditingDisabled: true,
          },
        ],
      },
    ],
  };
  return payload;
};

export const createEnvelope = async (address: string, state: any) => {
  const payload = await createEnvelopePayload(
    address,
    state.signers,
    state.blocks,
  );
  console.log('payload', payload);
  const json = JSON.stringify(payload);
  return await fetch('https://integ-digisign3.skyslope.com/api/envelopes', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization:
        'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJBY2NvdW50SWQiOiI2ZjIwYzFhMS1lZTJkLTRkZDktOTU5OS1kN2NjZGM1ODMzYzIiLCJVc2VyVHlwZSI6IlNlbmRlciIsInN1YiI6ImUwMGRjNDY5LWI0OWYtNDRiMC1iMzg4LWU3NjRhOTQyN2UwOSIsIkVtYWlsIjoiMTIzZGV2YnJva2VyQHNreXNsb3BlLmNvbSIsIlRva2VuVHlwZSI6IlNlbmRlclRva2VuIiwiVXNlcklkIjoiZTAwZGM0NjktYjQ5Zi00NGIwLWIzODgtZTc2NGE5NDI3ZTA5IiwiVXNlclJvbGUiOiJNZW1iZXIiLCJGaXJzdE5hbWUiOiJEZXYiLCJNaWRkbGVOYW1lIjoiIiwiTGFzdE5hbWUiOiJCcm9rZXIiLCJTZW5kZXJFbWFpbCI6IjEyM2RldmJyb2tlckBza3lzbG9wZS5jb20iLCJuYmYiOjE1ODY1NTg5MTgsImV4cCI6MTU4NzE2MzcxOCwiaWF0IjoxNTg2NTU4OTE4LCJpc3MiOiJodHRwczovL2RpZ2lzaWduMy5za3lzbG9wZS5jb20ifQ.J4E9Z-66vKoehWqe_QCjyB34c1ViM-MhoRFiQu0g28gHve7afbcwNtLzGyQAJAj6bWTbKGAbW9JhVJNO6Rn-gXyHjjFtYJWa0tnjHHpOCvGSQ0usqAbYXVATHAh7ZtuxMoQNQpLqKxnrDN9MIwDy89zAyVYzfsHvFgIHc6QEhthbair23yTzQ76NQG4hoL2X9Uen27gLpAa4dHWuY8S77xd6v4ALEXzPnW_5VQD6EVMDivtyDgHHGzAYEsiuXvwSmcMdt4XfoUx9yBn11tm4GbgMeobxcs90fcbDskZMUQy_zn2wI_2YwsXIlVJ8rEM7ePxznPRR7ZSOLJPdWFDurQ',
    },
    body: json,
  });
};
