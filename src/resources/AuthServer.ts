class AuthServer {
  public async authenticate(
    username: String,
    password: String,
  ): Promise<string> {
    const url = 'https://integ-auth.skyslope.com/identity/connect/token';

    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: `grant_type=password&username=${username}&password=${password}&scope=primeapi&client_id=ios-electron&client_secret=secret`,
    });
    return (await response.json()) || '';
  }
}

export default new AuthServer();
