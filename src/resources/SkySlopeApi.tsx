export const getFiles = async (token: string) => {
  const url =
    'https://api-integ.skyslope.com/api/files?createdAfter=2020-04-01&type=listing';
  const response = await fetch(url, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`,
      'Auth-Secondary': 'true',
    },
  });

  return response.json();
}

