import React from 'react';
import {View, Text} from 'react-native';
import {Item, Input, Icon, Button} from 'native-base';
import {connect} from 'react-redux';
import {addSigner as addSignerAction} from '../../store/digisign/actions';

const AddSigner = (props: any) => {
  const [name, setName] = React.useState('');
  const [email, setEmail] = React.useState('');

  const addSigner = () => {
    const names = name.split(/\s+/);
    if (name.length < 2) {
      names.push('');
    }
    props.dispatch(
      addSignerAction({
        signerId: '1234',
        email,
        firstName: names.shift(),
        middleName: '',
        lastName: names.pop(),
        group: 0,
        order: 0,
      }),
    );
    props.navigation.pop();
  };

  return (
    <View
      style={{
        flex: 1,
        padding: 24,
        backgroundColor: '#F4F8FC',
        justifyContent: 'flex-start',
      }}>
      <Text
        style={{
          color: '#03445F',
          fontFamily: 'Baskerville-SemiBold',
          fontSize: 36,
          letterSpacing: 0,
          paddingBottom: 24,
        }}>
        New Signer
      </Text>
      <Item>
        {/* <Icon active name="user" type="Feather" /> */}
        <Input
          autoCompleteType="name"
          placeholder="Name"
          value={name}
          onChangeText={(t) => setName(t)}
        />
      </Item>
      <Item>
        {/* <Icon active name="email-outline" type="MaterialCommunityIcons" /> */}
        <Input
          autoCapitalize="none"
          autoCompleteType="email"
          placeholder="Email"
          value={email}
          onChangeText={(t) => setEmail(t)}
        />
      </Item>
      <Button
        block
        style={{backgroundColor: '#0059DA', marginTop: 24}}
        onPress={addSigner}>
        <Text style={{color: 'white'}}>Add</Text>
      </Button>
    </View>
  );
};

export default connect(null)(AddSigner);
