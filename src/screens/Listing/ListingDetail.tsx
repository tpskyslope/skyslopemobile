import React from 'react';
import {Container, Text, Button, Badge} from 'native-base';
import {StyleSheet, View, ScrollView} from 'react-native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {
  faFileAlt,
  faPlusCircle,
  faEllipsisH,
  faPencilAlt
} from '@fortawesome/free-solid-svg-icons';
import {useNavigation} from '@react-navigation/native';

interface ListinDetailProps {
  //Route is from React Navigation. Use it to grab props passed to this component when navigated to.
  route: any;
}

const ListingDetail: React.FC<ListinDetailProps> = ({route}) => {
  console.log(`${route.params.listingData}`);
  const listingData = route.params.listingData;
  const navigation = useNavigation();
  return (
    <Container style={styles.container}>
      <ScrollView>
        <View>
          <View style={styles.listingHeader}>
            <Text
              style={
                styles.addressLine1Font
              }>{`${listingData.property.streetNumber} ${listingData.property.streetAddress}`}</Text>
            <Text
              style={
                styles.addressLine2Font
              }>{`${listingData.property.city}, ${listingData.property.state} ${listingData.property.zip}`}</Text>

            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <Text style={styles.listingHeaderTitle}>TYPE</Text>
                <Text>Listing</Text>
              </View>

              <View style={{flex: 1}}>
                <Text style={styles.listingHeaderTitle}>STATUS</Text>
                <Text>{`${listingData.status}`}</Text>
              </View>

              <View style={{flex: 1}}>{/*I'm just a flex place holder*/}</View>

              <View style={{flex: 1}}>
                <Text style={styles.listingHeaderTitle}>DOCS</Text>
                <Text>1</Text>
              </View>
            </View>
          </View>

          <View style={styles.sectionContainer}>
            <Text style={styles.sectionTitle}>LISTING INFORMATION</Text>

            <View style={styles.sectionCard}>
              <View style={{flexDirection: 'row'}}>
                <View>
                  <Text style={styles.sectionCardTitle}>Listing Price</Text>
                  <Text>
                    {new Intl.NumberFormat('en-US', {
                      style: 'currency',
                      currency: 'USD',
                    }).format(listingData.listingPrice)}
                  </Text>
                </View>

                <FontAwesomeIcon
                  style={{color: '#afbecd', marginTop: 24, marginLeft: 'auto'}}
                  size={16}
                  icon={faPencilAlt}
                />
              </View>

              <Text style={styles.sectionCardTitle}>Seller</Text>
              <Text>{`${listingData.sellers[0]?.firstName} ${listingData.sellers[0]?.lastName}`}</Text>

              <Text style={styles.sectionCardTitle}>Listing Date</Text>
              <Text>{`${new Date(listingData.listingDate).toLocaleDateString(
                'en-US',
                {
                  weekday: 'long',
                  year: 'numeric',
                  month: 'long',
                  day: 'numeric',
                },
              )}`}</Text>

              <Text style={styles.sectionCardTitle}>Checklist Type</Text>
              <Text>{`${listingData.type}`}</Text>
            </View>
          </View>

          <View style={styles.sectionContainer}>
            <View style={{flexDirection: 'row', marginRight:40}}>
              <Text style={styles.sectionTitle}>DOCUMENTS</Text>
              <Button
                transparent
                onPress={() => navigation.navigate('Documents', listingData)}
                style={{height: 25, marginLeft: 'auto'}}>
                <FontAwesomeIcon
                  style={{color: '#afbecd'}}
                  size={25}
                  icon={faPlusCircle}
                />
              </Button>
            </View>

            <View style={styles.sectionCard}>
              <View style={styles.documentCard}>
                <FontAwesomeIcon
                  style={{color: '#afbecd'}}
                  size={25}
                  icon={faFileAlt}
                />

                <View style={styles.documentCardText}>
                  <Text>Seller's Agreement</Text>
                  <Badge style={{backgroundColor: '#e1f1ff', borderRadius: 5, marginTop: 16}}>
                    <Text style={{color: '#0059da', fontWeight: 'bold', fontSize: 12}}>In-Progress</Text>
                  </Badge>
                </View>

                <FontAwesomeIcon
                  style={{color: '#afbecd', marginLeft: 'auto'}}
                  size={16}
                  icon={faEllipsisH}
                />
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F4F8FC',
  },
  listingHeader: {
    backgroundColor: '#FFF',
    padding: 24,
  },
  addressLine1Font: {
    color: '#3f5b77',
    fontSize: 40,
    fontFamily: 'Baskerville-SemiBold',
    fontWeight: 'bold',
  },
  addressLine2Font: {
    color: '#3f5b77',
    fontSize: 16,
  },
  listingHeaderTitle: {
    color: '#afbecd',
    fontSize: 12,
    fontWeight: 'bold',
    marginTop: 20,
  },
  sectionContainer: {
    marginTop: 24,
  },
  sectionTitle: {
    color: '#3f5b77',
    fontSize: 16,
    fontWeight: 'bold',
    marginLeft: 40,
  },
  sectionCard: {
    backgroundColor: '#FFF',
    margin: 16,
    paddingHorizontal: 24,
    paddingBottom: 24,
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 2,
      height: 4,
    },
    shadowOpacity: 0.23,
    shadowRadius: 5,
    elevation: 3,
  },
  sectionCardTitle: {
    color: '#3f5b77',
    marginTop: 16,
  },
  documentCard: {
    paddingTop: 24,
    flexDirection: 'row',
  },
  documentCardText: {
    paddingHorizontal: 16,
  },
});

export default ListingDetail;
