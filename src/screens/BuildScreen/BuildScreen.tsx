import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  TouchableWithoutFeedback,
  Button as StockButton,
  Alert,
} from 'react-native';
import {Button, Fab, Icon, Spinner, Toast} from 'native-base';
import {connect} from 'react-redux';
import Pdf from 'react-native-pdf';
import {CommonActions, useNavigation} from '@react-navigation/native';
import {
  addBlock as addBlockAction,
  resetState,
} from '../../store/digisign/actions';
// import {createEnvelope} from '../lib/server';
import {RootState} from '../../store';
import {createEnvelope} from '../../resources/EnvelopeServer';

const BuildScreen = (props: any) => {
  const navigation = useNavigation();
  const [fabOpen, setFabOpen] = React.useState(false);
  const [addingBlock, setAddingBlock] = React.useState(false);
  const [loading, setLoading] = useState(false);
  const {state} = props;
  const clickBlockType = () => {
    setFabOpen(false);
    setAddingBlock(true);
  };

  const touchDocument = () => {
    if (addingBlock) {
      setAddingBlock(false);
      props.dispatch(
        addBlockAction({
          pageNumber: 0,
          blockType: 'Signature',
          value: null,
          x: 37,
          y: 576,
          width: 258,
          height: 22,
          required: true,
          assignedTo: state.signers[0].signerId,
        }),
      );
    }
  };

  const send = async () => {
    setLoading(true);
    const res = await createEnvelope(getAddress(), props.state);
    const result = await res.json();
    console.log('result', result);
    setLoading(false);
    Alert.alert('Envelope Created!', '');
    navigation.navigate('Home');
    // props.navigation.reset([CommonActions.navigate({name: 'Saving'})], 0);
    // console.log('sending with state', state);
    // const res = await createEnvelope(state);
    // console.log('res is', res);
    // props.navigation.reset([CommonActions.navigate({name: 'Done'})], 0);
  };

  const getAddress = (): string => {
    return `${props.state.currentFile.property.streetNumber} ${props.state.currentFile.property.streetAddress} ${props.state.currentFile.property.city} ${props.state.currentFile.property.state}`;
  };

  return (
    <TouchableWithoutFeedback onPress={touchDocument}>
      <View style={styles.container}>
        {state.documents.map((document: any) => (
          <Pdf
            key={document.uri}
            source={{uri: document.uri}}
            onError={(error: any) => {
              console.log(error);
            }}
            style={styles.pdf}>
            {addingBlock ? <View style={styles.overlay} /> : undefined}
          </Pdf>
        ))}
        {state.blocks
          ? state.blocks.map((block: any, index: number) => (
              <View style={styles.block} key={index}>
                <Text style={{color: 'black'}}>Signature</Text>
              </View>
            ))
          : undefined}
        <View style={styles.addressBlocK}>
          <Text style={{color: 'black', fontSize: 8}}>{getAddress()}</Text>
        </View>
        <Fab
          active={fabOpen}
          direction="up"
          style={styles.fab}
          position="bottomRight"
          onPress={() => setFabOpen(true)}>
          <Icon name="plus" type="MaterialCommunityIcons" />

          {fabOpen ? (
            <Button style={styles.fab} onPress={clickBlockType}>
              <Icon name="pen" type="MaterialCommunityIcons" />
            </Button>
          ) : undefined}
          {fabOpen ? (
            <Button style={styles.fab}>
              <Icon name="sword" type="MaterialCommunityIcons" />
            </Button>
          ) : undefined}
        </Fab>
        {state.blocks.length ? (
          <Fab style={styles.fab} position="bottomLeft" onPress={send}>
            {loading && <Spinner />}
            {!loading && <Icon name="send" type="MaterialIcons" />}
          </Fab>
        ) : undefined}
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#F4F8FC',
  },
  pdf: {
    flex: 1,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  fab: {
    backgroundColor: '#0059DA',
  },
  overlay: {
    width: '100%',
    height: '100%',
    backgroundColor: '#0059DA',
    opacity: 0.2,
  },
  addressBlocK: {
    fontSize: 1,
    position: 'absolute',
    left: 25,
    top: 575,
    height: 12,
    width: 180,
  },
  block: {
    backgroundColor: '#0059DA',
    opacity: 0.2,
    position: 'absolute',
    left: 20,
    top: 520,
    height: 16,
    width: 180,
  },
});

export default connect((state: RootState) => ({state: state.digiSignState}))(
  BuildScreen,
);
