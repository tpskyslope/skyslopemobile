import React, {useEffect} from 'react';
import {View, Text} from 'react-native';
import {Button, Card, CardItem, Body} from 'native-base';
import DocumentPicker from 'react-native-document-picker';
import {connect} from 'react-redux';
import {addDocument, setCurrentFile} from '../../store/digisign/actions';

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faPlusCircle} from '@fortawesome/free-solid-svg-icons';
import {RootState} from '../../store';
import DocumentCard from '../../Components/DocumentCard';
import {useRoute} from '@react-navigation/native';

const Documents = (props: any) => {
  const {params} = useRoute();

  useEffect(() => {
    props.dispatch(setCurrentFile(params));
  });

  const pickFile = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.pdf],
      })!;
      props.dispatch(addDocument(res));
    } catch (err) {
      if (!DocumentPicker.isCancel(err)) {
        throw err;
      }
    }
  };

  return (
    <View
      style={{
        flex: 1,
        padding: 24,
        backgroundColor: '#F4F8FC',
        justifyContent: 'flex-start',
      }}>
      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <Text
          style={{
            color: '#03445F',
            fontFamily: 'Baskerville-SemiBold',
            fontSize: 36,
            letterSpacing: 0,
            paddingBottom: 24,
          }}>
          Documents
        </Text>
        <Button iconLeft transparent onPress={pickFile}>
          <FontAwesomeIcon size={25} icon={faPlusCircle} />
        </Button>
      </View>
      <View style={{flexGrow: 1}}>
        {props.documents.map((doc: any) => (
          <DocumentCard doc={doc} />
        ))}
      </View>
      {props.documents.length ? (
        <Button
          block
          style={{backgroundColor: '#0059DA'}}
          onPress={() => props.navigation.push('Signers')}>
          <Text style={{color: 'white'}}>Add Signers</Text>
        </Button>
      ) : undefined}
    </View>
  );
};

export default connect((state: RootState) => ({
  documents: state.digiSignState.documents,
}))(Documents);
