import React, {useState} from 'react';
import {Image, Text, StyleSheet, ActivityIndicator} from 'react-native';
import {
  Container,
  Content,
  Form,
  Item,
  Input,
  Label,
  Button,
} from 'native-base';
import {connect} from 'react-redux';
import AuthServer from '../../resources/AuthServer';
import {setAuthToken} from '../../store/auth';

const styles = StyleSheet.create({
  loginButton: {
    flex: 1,
    margin: 16,
    justifyContent: 'center',
  },
});

export interface LoginScreenProps {
  dispatchSetAuthToken: (token: string) => void;
}

const LoginScreen: React.FC<LoginScreenProps> = ({dispatchSetAuthToken}) => {
  const [userName, setUserName] = useState<string>('123devbroker@skyslope.com');
  const [password, setPassword] = useState<string>('A123456');
  const [loading, setLoading] = useState<boolean>(false);

  const handleLogin = async () => {
    try {
      setLoading(true);
      const response = await AuthServer.authenticate(userName, password);
      setLoading(false);
      dispatchSetAuthToken(response);
    } catch (e) {
      console.log('error', e);
      setLoading(false);
    }
  };

  return (
    <Container>
      <Content>
        <Image
          source={require('./../../../assets/skyslope_logo.png')}
          resizeMode="center"
          style={{
            alignSelf: 'center',
          }}
        />
        <Form>
          <Item stackedLabel>
            <Label>Username</Label>
            <Input
              onChangeText={(e) => setUserName(e)}
              defaultValue="123devbroker@skyslope.com"
            />
          </Item>
          <Item stackedLabel>
            <Label>Password</Label>
            <Input
              onChangeText={(e) => setPassword(e)}
              secureTextEntry
              defaultValue="A123456"
            />
          </Item>
        </Form>
        <Button style={styles.loginButton} onPressOut={handleLogin}>
          {loading && <ActivityIndicator size="small" />}
          {!loading && <Text style={{color: 'white'}}>Login</Text>}
        </Button>
      </Content>
    </Container>
  );
};

export default connect(() => ({}), {
  dispatchSetAuthToken: setAuthToken,
})(LoginScreen);
