import React, {useState, useEffect} from 'react';
import {Container, Fab, Icon} from 'native-base';
import bulkData from '../../resources/mock/mockbulk';
import {StyleSheet, ScrollView, View} from 'react-native';
import ListingCard from '../../Components/ListingCard';
import {SearchBar} from 'react-native-elements';
import {resetState} from '../../store/digisign/actions';
import {connect} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

interface HomeScreenProps {}

const HomeScreen: React.FC<HomeScreenProps> = (props: any) => {
  const [listingArr, setListingArr] = useState(bulkData.value);
  const [search, setSearch] = useState('');
  const navigation = useNavigation();

  const handleSearch = (e: any) => {
    if (e === '') {
      setListingArr(bulkData.value);
    } else {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      setListingArr(
        bulkData.value.filter((item) => {
          const addy = `${item.property.streetNumber} ${item.property.streetAddress}`;
          if (addy.includes(e)) {
            return item;
          }
          return false;
        }),
      );
    }

    setSearch(e);
  };

  useEffect(() => {
    props.dispatch(resetState());
  });
  return (
    <Container style={styles.container}>
      <View>
        <SearchBar
          lightTheme
          platform="default"
          placeholder=""
          onChangeText={handleSearch}
          value={search}
          style={{backgroundColor: '#F4F8FC'}}
        />
      </View>
      <ScrollView>
        {listingArr.map((item: any) => (
          <ListingCard item={item} />
        ))}
      </ScrollView>
      <Fab onPress={() => navigation.navigate('Create')}>
        <Icon name="add" />
      </Fab>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F4F8FC',
  },
});

export default connect(() => ({}))(HomeScreen);
