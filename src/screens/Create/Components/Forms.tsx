import React, { useState } from 'react'
import {StyleSheet} from 'react-native';
import {Card, CardItem, Grid, Row, Col, CheckBox, Text, H1} from 'native-base';

interface Form {
  name: string
}

export interface Forms extends Array<Form> {
}

interface FormsStepProps {
  forms: Array<Form>;
  setForms: (forms: Array<Form>) => void;
}

const formsMock = [
  'Residential Purchase Agreement',
  'Buyer Broker Agreement',
  'Inspection Contingency Removal',
  'Purchase Addendum',
  'Appraisal Contingency Removal'
]

const FormsStep: React.FC<FormsStepProps> = ({ forms, setForms }) => {
  const isInForms = (form: string): boolean => {
    return !forms.every(inForm => inForm.name !== form)
  }
  const toggleInForms = (form: string): void => {
    if (isInForms(form)) {
      setForms(forms.filter(inForm => inForm.name !== form))
    } else {
      setForms([...forms, { name: form }])
    }
  }
  return (
    <>
      <H1 style={styles.title}>What forms would you like to add?</H1>
      {formsMock.map(form => {
        return (
          <Card style={styles.card} onTouchEnd={() => toggleInForms(form)}>
            <CardItem>
              <Grid>
                <Row>
                  <Col size={1}>
                    <CheckBox checked={isInForms(form)} onPress={() => toggleInForms(form)} />
                  </Col>
                  <Col size={9}>
                    <Row>
                      <Text style={styles.formLabel}>
                        {form}
                      </Text>
                    </Row>
                    <Row>
                      <Text style={styles.formFor}>Arizona | Buyer</Text>
                    </Row>
                  </Col>
                </Row>
              </Grid>
            </CardItem>
          </Card>
        )
      })}
    </>
  )
}

const styles = StyleSheet.create({
  title: {
    marginBottom: 40,
    color: '#415D79',
    fontFamily: 'Baskerville-SemiBold'
  },
  card: {
    paddingTop: 16,
    paddingBottom: 16,
    marginBottom: 16
  },
  formLabel: {
    marginLeft: 12,
    fontSize: 14,
    fontWeight: 'bold',
    marginBottom: 6
  },
  formFor: {
    marginLeft: 12,
    color: '#bebebe',
    fontSize: 12
  }
});

export default FormsStep