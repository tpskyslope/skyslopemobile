import React, { useState } from 'react'
import {StyleSheet} from 'react-native';
import {Grid, Row, Col, Card, CardItem, Body, Text, Item, Input, H1} from 'native-base';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faMapMarkerAlt, faHome, faTimes} from '@fortawesome/free-solid-svg-icons';

const addresses: Array<Address> = [
  {
    street: '2713 Boris Johnson Way',
    city: 'Woodland',
    state: 'Ca',
    zip: '95776'
  },
  {
    street: '2716 Zane Dr.',
    city: 'Woodland',
    state: 'Ca',
    zip: '95776'
  },
  {
    street: '2716 Sweeney Dr.',
    city: 'Woodland',
    state: 'Ca',
    zip: '95776'
  },
  {
    street: '2716 Banks Dr.',
    city: 'Woodland',
    state: 'Ca',
    zip: '95776'
  }
]

export interface Address {
  street: string;
  city: string;
  state: string;
  zip: string;
}

interface AddressChooserProps {
  addresses: Address[];
  search: string;
  onChoose: (address: Address) => void;
}

const AddressChooser: React.FC<AddressChooserProps> = ({ addresses, search, onChoose }) => {
  const filteredAddresses = addresses.filter(address => address.street.includes(search))
  if (!search) {
    return null
  }
  return (
    <Card>
      <CardItem>
        <Body>
          {filteredAddresses.map((address, index) => {
            return (
              <Grid onTouchEnd={() => onChoose(address)} style={(index === filteredAddresses.length - 1) ? styles.addressLast : styles.address}>
                <Row>
                  <Text style={styles.street}>
                    {address.street}
                  </Text>
                </Row>
                <Row>
                  <Text>
                    {address.city}, {address.state} {address.zip}
                  </Text>
                </Row>
              </Grid>
            )
          })}
        </Body>
      </CardItem>
    </Card>
  )
}

interface ChosenAddressProps {
  address: Address;
  onCancel: () => void;
}

const ChosenAddress: React.FC<ChosenAddressProps> = ({ address, onCancel }) => {
  return (
    <Card>
      <CardItem>
        <Body>
          <Grid style={styles.addressLast}>
            <Col size={10}>
              <FontAwesomeIcon
                style={styles.icon}
                size={18}
                icon={faHome}
              />
            </Col>
            <Col size={70}>
              <Row>
                <Text style={styles.street}>
                  {address.street}
                </Text>
              </Row>
              <Row>
                <Text>
                  {address.city}, {address.state} {address.zip}
                </Text>
              </Row>
            </Col>
            <Col size={20}>
              <Row onTouchEnd={onCancel} style={{ justifyContent: 'flex-end' }}>
                <FontAwesomeIcon
                  style={styles.icon}
                  size={18}
                  icon={faTimes}
                />
              </Row>
            </Col>
          </Grid>
        </Body>
      </CardItem>
    </Card>
  )
}

interface AddressStepProps {
  address: Address | undefined;
  setAddress: (address: Address | undefined) => void;
}

const AddressStep: React.FC<AddressStepProps> = ({ address, setAddress }) => {
  const [search, setSearch] = useState<string>('');

  return (
    <>
      {!!address && (
        <ChosenAddress address={address} onCancel={() => setAddress(undefined)} />
      )}
      {!!!address && (
        <>
          <H1 style={styles.title}>What is the property's address?</H1>
          <Item style={styles.search} regular>
            <Input placeholder='' onChangeText={e => setSearch(e)} defaultValue='' />
            <FontAwesomeIcon
              style={styles.icon}
              size={25}
              icon={faMapMarkerAlt}
            />
          </Item>
          <AddressChooser addresses={addresses} search={search} onChoose={setAddress} />
        </>
      )}
    </>
  )
}

const styles = StyleSheet.create({
  street: {
    fontWeight: 'bold'
  },
  address: {
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: '#efefef',
    padding: 16
  },
  addressLast: {
    width: '100%',
    padding: 16
  },
  search: {
    borderRadius: 8
  },
  icon: {
    color: '#415D79',
    marginRight: 10
  },
  title: {
    marginBottom: 40,
    color: '#415D79',
    fontFamily: 'Baskerville-SemiBold'
  }
});

export default AddressStep
