import React, { useState } from 'react'
import {StyleSheet} from 'react-native';
import {Label, Grid, Row, Col, Radio, Text, Item, Input, H1, H3} from 'native-base';

interface Contact {
  first: string;
  last: string;
  middle: string;
}

export interface Contacts extends Array<Contact> {

}

interface ContactsStepProps {
  contacts: Contacts;
  setContacts: (contacts: Contacts) => void;
}

const types = [
  'Buyer',
  'Seller',
  'Both Buyer and Seller',
  'Tenant',
  'Landlord',
  'Both Tenant and Landord'
]

const ContactsStep: React.FC<ContactsStepProps> = ({ contacts, setContacts }) => {
  const [type, setType] = useState<string>('')
  return (
    <>
      <H1 style={styles.title}>Who are you representing?</H1>

      <H3>Type</H3>
      <Grid style={styles.type}>
        {types.map(typeLabel => {
          return (
            <Row style={styles.typeRow} onTouchEnd={() => setType(typeLabel)}>
              <Radio selected={typeLabel === type} />
              <Text style={styles.typeLabel}>{typeLabel}</Text>
            </Row>
          )
        })}
      </Grid>

      <H3>Primary client</H3>
      <Label style={styles.label}>First Name</Label>
      <Item style={styles.input} regular>
        <Input placeholder='' />
      </Item>
      <Label style={styles.label}>Middle Name</Label>
      <Item style={styles.input} regular>
        <Input placeholder='' />
      </Item>
      <Label style={styles.label}>Last Name</Label>
      <Item style={styles.input} regular>
        <Input placeholder='' />
      </Item>
    </>
  )
}

const styles = StyleSheet.create({
  type: {
    marginBottom: 24,
    marginTop: 24
  },
  typeRow: {
    marginBottom: 12
  },
  typeLabel: {
    marginLeft: 12
  },
  icon: {
    color: '#415D79',
    marginRight: 10
  },
  input: {
    borderRadius: 8
  },
  title: {
    marginBottom: 40,
    color: '#415D79',
    fontFamily: 'Baskerville-SemiBold'
  },
  label: {
    marginTop: 24,
    marginLeft: 6,
    marginBottom: 6
  }
});

export default ContactsStep