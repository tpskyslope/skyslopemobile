import React, {useEffect, useState} from 'react';
import {Container, Toast, Left, Right, Button, Text, Content, Footer} from 'native-base';
import {ImageBackground, StyleSheet, Alert} from 'react-native';
import {resetState} from '../../store/digisign/actions';
import {connect} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

interface CreateProps {}

import { Address } from './Components/Address'
import AddressStep from './Components/Address'

import { Contacts } from './Components/Contacts'
import ContactsStep from './Components/Contacts'

import { Forms } from './Components/Forms'
import FormsStep from './Components/Forms'

const ADDRESS_STEP = 1;
const CONTACTS_STEP = 2;
const FORMS_STEP = 3;

const Create: React.FC<CreateProps> = (props: any) => {
  const [address, setAddress] = useState<Address|undefined>();
  const [contacts, setContacts] = useState<Contacts>([]);
  const [forms, setForms] = useState<Forms>([]);
  const [step, setStep] = useState(ADDRESS_STEP);
  const navigation = useNavigation();

  useEffect(() => {
    props.dispatch(resetState());
  });

  useEffect(() => {
    if (step > 3) {
      navigation.navigate('Home');
      Toast.show({
        text: 'New File Created!',
        buttonText: 'Ok',
        position: 'top',
        duration: 3000,
        type: "success",
        style: {marginTop: 70}
      })
    }
  }, [step]);

  const isNextDisabled = () => {
    if (step === ADDRESS_STEP) {
      return!!!address
    }
    return false
  }

  const addressImg = require('./../../../assets/home.png')

  return (
    <Container style={styles.container}>
      <ImageBackground
        source={addressImg}
        imageStyle={{
          resizeMode: "contain",
          alignSelf: "flex-end",
          bottom: step === ADDRESS_STEP ? -454 : 10000
        }}
        style={{
          flex: 1,
          justifyContent: "flex-end"
        }}
      >
        <Content style={styles.content}>
          {step === ADDRESS_STEP && <AddressStep address={address} setAddress={setAddress} />}
          {step === CONTACTS_STEP && <ContactsStep contacts={contacts} setContacts={setContacts} />}
          {step === FORMS_STEP && <FormsStep forms={forms} setForms={setForms} />}
        </Content>
      </ImageBackground>
      <Footer style={styles.footer}>
        <Left>
          <Text>
            {step !== FORMS_STEP && `Continue to step ${step + 1}`}
            {step === FORMS_STEP && 'Continue to Create File'}
          </Text>
        </Left>
        <Right>
          <Button rounded disabled={isNextDisabled()} onPress={() => { !isNextDisabled() && 
            setStep(step + 1) }}>
            <Text>
              {step === FORMS_STEP && 'Create File'}
              {step !== FORMS_STEP && 'Continue'}
            </Text>
          </Button>
        </Right>
      </Footer>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    flex: 1,
    flexDirection: "column"
  },
  content: {
    padding: 30,
    zIndex: 1
  },
  footer: {
    paddingTop: 20,
    paddingLeft: 30,
    paddingRight: 30,
    backgroundColor: '#FFF'
  }
});

export default connect(() => ({}))(Create);
