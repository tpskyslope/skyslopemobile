import React from 'react';
import {View, Text} from 'react-native';
import {Button, Card, CardItem, Body, Icon} from 'native-base';
import {connect} from 'react-redux';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faPlusCircle} from '@fortawesome/free-solid-svg-icons';
import {RootState} from '../../store';
import {useNavigation} from '@react-navigation/native';

const Signers = (props: any) => {
  const navigation = useNavigation();
  return (
    <View
      style={{
        flex: 1,
        padding: 24,
        backgroundColor: '#F4F8FC',
        justifyContent: 'flex-start',
      }}>
      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <Text
          style={{
            color: '#03445F',
            fontFamily: 'Baskerville-SemiBold',
            fontSize: 36,
            letterSpacing: 0,
            paddingBottom: 24,
          }}>
          Signers
        </Text>
        <Button
          iconLeft
          transparent
          onPress={() => props.navigation.push('AddSigner')}>
          <FontAwesomeIcon size={25} icon={faPlusCircle} />
        </Button>
      </View>
      <View style={{flexGrow: 1}}>
        {props.signers.map((signer: any) => (
          <Card key={signer.email}>
            <CardItem>
              <Body style={{padding: 12}}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  {/* <Icon active name="user" type="Feather" /> */}
                  <Text style={{paddingLeft: 12}}>
                    {signer.firstName} {signer.lastName}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingTop: 12,
                  }}>
                  <FontAwesomeIcon size={25} icon={faPlusCircle} />
                  <Text style={{paddingLeft: 12}}>{signer.email}</Text>
                </View>
              </Body>
            </CardItem>
          </Card>
        ))}
      </View>
      {props.signers.length ? (
        <Button
          block
          style={{backgroundColor: '#0059DA'}}
          onPress={() => navigation.navigate('Build')}>
          <Text style={{color: 'white'}}>Prepare Envelope</Text>
        </Button>
      ) : undefined}
    </View>
  );
};

export default connect((state: RootState) => ({
  signers: state.digiSignState.signers,
}))(Signers);
