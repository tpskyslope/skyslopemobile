import React from 'react';
import {Text, Button} from 'native-base';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faInfoCircle, faEllipsisH} from '@fortawesome/free-solid-svg-icons';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {StyleSheet, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';

const ListingCard = (props: any) => {
  const {item} = props;
  const navigation = useNavigation();
  return (
    <View style={styles.cardWrapper} key={item.listingGuid}>
      <TouchableWithoutFeedback
        onPress={() =>
          navigation.navigate('Listing', {
            listingData: item,
          })
        }>
        {/* Listing Info Wrapper*/}
        <View>
          {/* Property Address (If it exists) */}
          {item.property?.streetNumber && item.property?.streetAddress ? (
            <Text style={styles.address}>
              {`${item.property.streetNumber} ${item.property.streetAddress}`}
            </Text>
          ) : undefined}

          {/* Type of listing */}
          {/* <Text style={{fontFamily: 'system'}}>
            {item.objectType.charAt(0).toUpperCase() + item.objectType.slice(1)}
          </Text> */}

          <Text style={styles.listingStatus}>LISTING STATUS</Text>
          <Text style={{fontSize: 17}}>{item.status}</Text>

          {/* Seller Info */}
          {/* {item.sellers && item.sellers.length >= 1 ? (
            <Text>
              Seller:{' '}
              {`${item.sellers[0]?.firstName} ${item.sellers[0]?.lastName}`}
            </Text>
          ) : undefined} */}
        </View>
      </TouchableWithoutFeedback>

      {/* Icon Wrapper */}
      <View>
        <Button
          transparent
          onPress={() => navigation.navigate('Documents', item)}>
          <FontAwesomeIcon style={styles.icon} size={15} icon={faEllipsisH} />
        </Button>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F4F8FC',
  },
  listingStatus: {
    fontFamily: 'System',
    fontSize: 15,
    color: '#afbecd',
    fontWeight: 'bold',
  },
  address: {
    color: '#415D79',
    fontWeight: 'bold',
    fontSize: 30,
    fontFamily: 'Baskerville-SemiBold',
    marginBottom: 24,
  },
  cardWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    borderRadius: 7,
    padding: 24,
    marginVertical: 16,
    marginHorizontal: 16,
    shadowColor: '#000',
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2,
    elevation: 2,
  },
  icon: {
    color: '#AFBECD',
  },
});

export default ListingCard;
