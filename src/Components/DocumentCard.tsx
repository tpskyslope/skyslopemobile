import React from 'react';
import {Text} from 'react-native';
import {Card, CardItem, Body} from 'native-base';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faFile} from '@fortawesome/free-solid-svg-icons';

const DocumentCard = (props: any) => (
  <Card key={props.doc.uri}>
    <CardItem>
      <Body
        style={{
          padding: 24,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <FontAwesomeIcon size={25} icon={faFile} />
        <Text style={{paddingLeft: 12}}>{props.doc.name}</Text>
      </Body>
    </CardItem>
  </Card>
);

export default DocumentCard;
