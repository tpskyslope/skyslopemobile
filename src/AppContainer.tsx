import React, {useEffect} from 'react';
import {Button, Root} from 'native-base';
import {Text, Alert} from 'react-native';
import LoginScreen from './screens/Login/LoginScreen';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import ListingDetail from './screens/Listing/ListingDetail';
import HomeScreen from './screens/Home/HomeScreen';
import Documents from './screens/Documents/Documents';
import Signers from './screens/Signers/Signers';
import AddSigner from './screens/AddSigner/AddSigner';
import BuildScreen from './screens/BuildScreen/BuildScreen';
import Create from './screens/Create/Create';
import {connect} from 'react-redux';
import {RootState} from './store';
import {setAuthToken} from './store/auth';
import messaging, {firebase} from '@react-native-firebase/messaging';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faArrowLeft} from '@fortawesome/free-solid-svg-icons';

const Stack = createStackNavigator();

interface AppContainerProps {
  isAuthenticated: boolean;
  dispatchSetAuthToken: (token?: string) => void;
}

const AppContainer: React.FC<AppContainerProps> = ({
  isAuthenticated,
  dispatchSetAuthToken,
}) => {
  useEffect(() => {
    async function registerAppWithFCM() {
      await messaging().registerDeviceForRemoteMessages();
      const fcmToken = await firebase.messaging().getToken();
      console.log('fcmToken', fcmToken);
    }
    async function requestUserPermission() {
      const settings = await messaging().requestPermission();

      if (settings) {
        console.log('Permission settings:', settings);
      }
    }
    registerAppWithFCM();
    requestUserPermission();
  });

  useEffect(() => {
    const unsubscribe = messaging().onMessage(async (remoteMessage: any) => {
      console.log('dsss-message', remoteMessage);
      //TODO:: maybe we can use a toast or a better UI
      Alert.alert(
        remoteMessage?.data?.notification?.title,
        remoteMessage?.data?.notification?.body,
      );
    });

    return unsubscribe;
  }, []);

  if (!isAuthenticated) {
    return <LoginScreen />;
  }

  return (
    <Root>
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{
            headerRight: () => (
              <Button
                style={{backgroundColor: 'white', padding: 8}}
                onPressOut={() => {
                  dispatchSetAuthToken(undefined);
                }}>
                <Text>Logout</Text>
              </Button>
            ),
          }}
        />
        <Stack.Screen name="Listing" component={ListingDetail} options={{
            title: '',
            headerBackImage: () => (
              <FontAwesomeIcon
                style={{marginHorizontal: 24, color: '#415D79'}}
                size={25}
                icon={faArrowLeft}
              />
            ),
            headerBackTitle: ' ',
            headerStyle: {
              shadowColor: 'transparent',
              shadowRadius: 0,
              shadowOffset: {
                height: 0,
              },
            },
          }} />
        <Stack.Screen name="Documents" component={Documents} />
        <Stack.Screen name="Signers" component={Signers} />
        <Stack.Screen
          name="AddSigner"
          component={AddSigner}
          options={{
            title: "Add Signer"
          }}
        />
        <Stack.Screen name="Build" component={BuildScreen} />
        <Stack.Screen
          name="Create"
          component={Create}
          options={{
            title: 'Create File'
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
    </Root>
  );
};

const getAuthenticated = (state: RootState): boolean => {
  return state.authState.accessToken ? true : false;
};

export default connect(
  (state: RootState) => ({
    isAuthenticated: getAuthenticated(state),
  }),
  {
    dispatchSetAuthToken: setAuthToken,
  },
)(AppContainer);
