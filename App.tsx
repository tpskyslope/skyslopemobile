/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import 'react-native-gesture-handler';
import React from 'react';
import AppContainer from './src/AppContainer';
import {Provider} from 'react-redux';
import configureStore from './src/store';

const store = configureStore();

declare var global: {HermesInternal: null | {}};
console.disableYellowBox = true;
const App = () => {
  return (
    <Provider store={store}>
      <AppContainer />
    </Provider>
  );
};

export default App;
